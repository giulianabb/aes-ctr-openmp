/*********************************************************************
* Filename:   aes_test.c
* Author:     Brad Conte (brad AT bradconte.com)
* Copyright:
* Disclaimer: This code is presented "as is" without any guarantees.
* Details:    Performs known-answer tests on the corresponding AES
              implementation. These tests do not encompass the full
              range of available test vectors and are not sufficient
              for FIPS-140 certification. However, if the tests pass
              it is very, very likely that the code is correct and was
              compiled properly. This code also serves as
	          example usage of the functions.
*********************************************************************/


/*************************** HEADER FILES ***************************/
#include <stdio.h>
#include <memory.h>
#include "aes.h"
#include <sys/time.h>
#include "mede_time.h"

/*********************** FUNCTION DEFINITIONS ***********************/
void print_hex(BYTE str[], int len)
{
	int idx;

	for(idx = 0; idx < len; idx++)
		printf("%02x", str[idx]);
}

int ascii_to_hex(char c)
{
        int num = (int) c;
        if(num < 58 && num > 47)
        {
                return num - 48; 
        }
        if(num < 103 && num > 96)
        {
                return num - 87;
        }
        return num;
}

void open_hex_file(int size, BYTE matriz[1][size], char *name) {
	FILE *fp = fopen(name,"r");
    unsigned char c1,c2;
    int i=0;
    unsigned char sum;
    for(i=0;i<MESSAGE_SIZE;i++)
    {
            c1 = ascii_to_hex(fgetc(fp));
            c2 = ascii_to_hex(fgetc(fp));
            sum = c1<<4 | c2;
            matriz[0][i] = sum;
    }
}

void write_hex_file(int size, BYTE matriz[size], char *name) {
	FILE *fp = fopen(name,"wb");
   
    int i=0;
    for(i=0;i<MESSAGE_SIZE;i++){
        fprintf(fp, "%02x", matriz[i]);
    }

	fclose(fp);
}

int aes_ctr_test()
{
	WORD key_schedule[60];
	BYTE enc_buf[MESSAGE_SIZE];
	BYTE plaintext[1][MESSAGE_SIZE];
	BYTE ciphertext[1][MESSAGE_SIZE];
	BYTE iv[1][16] = {
		{0xf0,0xf1,0xf2,0xf3,0xf4,0xf5,0xf6,0xf7,0xf8,0xf9,0xfa,0xfb,0xfc,0xfd,0xfe,0xff},
	};
	BYTE key[1][32] = {
		{0x60,0x3d,0xeb,0x10,0x15,0xca,0x71,0xbe,0x2b,0x73,0xae,0xf0,0x85,0x7d,0x77,0x81,0x1f,0x35,0x2c,0x07,0x3b,0x61,0x08,0xd7,0x2d,0x98,0x10,0xa3,0x09,0x14,0xdf,0xf4}
	};
	int i, pass = 1;
	float tempo_somado = 0;
	
	open_hex_file(MESSAGE_SIZE, plaintext, "input.txt");
	open_hex_file(MESSAGE_SIZE, ciphertext, "output.txt");
	
	
	printf("* CTR mode:\n");
	aes_key_setup(key[0], key_schedule, 256);

	printf(  "Key          : ");
	print_hex(key[0], 32);
	printf("\nIV           : ");
	print_hex(iv[0], 16);

	printf("\n\nExecutando para %d threads\n", NUM_THREADS);

	TIMER_CLEAR;
	for(i = 0; i < BETA; i++) {
		#if NUM_THREADS == 1
		TIMER_START;
	  	aes_encrypt_ctr_linear(plaintext[0], MESSAGE_SIZE, enc_buf, key_schedule, 256, iv[0]);
	  	TIMER_STOP;
		tempo_somado += TIMER_ELAPSED;
		#else
	  	TIMER_START;
	  	aes_encrypt_ctr(plaintext[0], MESSAGE_SIZE, enc_buf, key_schedule, 256, iv[0]);
	  	TIMER_STOP;
		tempo_somado += TIMER_ELAPSED;
		#endif
	}


	printf("\n\nPlaintext    : ");
	print_hex(plaintext[0], 20);
	printf("\n\n-encrypted to: ");
	print_hex(&enc_buf[MESSAGE_SIZE-21], 20);
	printf("\n\nexpected: ");
	print_hex(&ciphertext[0][MESSAGE_SIZE-21], 20);
	

	write_hex_file(MESSAGE_SIZE, enc_buf, "output2.txt");
	
	pass = pass && !memcmp(enc_buf, ciphertext[0], MESSAGE_SIZE);


	// aes_decrypt_ctr(ciphertext[0], MESSAGE_SIZE, enc_buf, key_schedule, 256, iv[0]);
	// printf("\n\nCiphertext   : ");
	// print_hex(ciphertext[0], MESSAGE_SIZE);
	// printf("\n\n-decrypted to: ");
	// print_hex(enc_buf, MESSAGE_SIZE);
	// pass = pass && !memcmp(enc_buf, plaintext[0], MESSAGE_SIZE);

  	printf ("\n\nTEMPO MÉDIO PARA CIFRAR: %12.7f\n\n", tempo_somado/BETA);
	return(pass);
}

int aes_test()
{
	int pass = 1;

	pass = pass && aes_ctr_test();

	return(pass);
}

int main(int argc, char *argv[])
{
	printf("AES CTR Test: %s\n", aes_test() ? "SUCCEEDED" : "FAILED");

	return(0);
}

