================================
			Tempos
================================


Utilizando sections para arquivos grandes.
BETA = 5000


Arquivo com 262.2 kB (131.088 posições no vetor de BYTE)

- Sem paralelismo: 0.0276681   

- 2 threads: 0.0147245

- 4 threads: 0.0074887

- 8 threads: 0.0038300

- 16 threads: 0.0036730 (possível concorrência - 16 threads máximo)



Arquivo com 524.3 kB (262.160 posições no vetor de BYTE)

- Sem paralelismo: 0.0550102

- 2 threads: 0.0300172

- 4 threads: 0.0145063

- 8 threads:  0.0077828


Arquivo com 1 MB (524.304 posições no vetor de BYTE)

- Sem paralelismo: 0.1099636 (menos)

- 2 threads: 0.0557607

- 4 threads: 0.0293932

- 8 threads: 0.0153593

