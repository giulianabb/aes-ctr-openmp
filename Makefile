CC=gcc
CFLAGS=-I. -fopenmp
DEPS = aes.h mede_time.h
OBJ = aes_test.o aes.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

aes_test: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)


.PHONY: clean

clean:
	rm -f *.o *~ core $(INCDIR)/*~ 